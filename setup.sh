#!/bin/bash

# curl -sSL https://gitlab.com/philipp/silverstripe-k8s-boilerplate/raw/master/setup.sh | bash
# set -e

git clone https://gitlab.com/philipp/silverstripe-k8s-boilerplate.git temp
mv temp/{.,}* .
composer install
echo -e "\n\
SS_DATABASE_CLASS='PostgreSQLDatabase'\n\
SS_DATABASE_NAME=$SS_DATABASE_NAME\n\
SS_DATABASE_PASSWORD=$SS_DATABASE_PASSWORD\n\
SS_DATABASE_SERVER=$SS_DATABASE_SERVER\n\
SS_DATABASE_USERNAME=$SS_DATABASE_USERNAME\n\
SS_DATABASE_PORT=$SS_DATABASE_PORT\n\
SS_ENVIRONMENT_TYPE='live'\n\
" > .env
